# Présentations

## R Shiny Apps serveurs

  * 09/09/2019 Shiny Apps, comment déployer son App R Shiny [Presentation](RShinyApps/JF_REY_RShinyApp.pdf) 

## R Parallèle

  * 08/12/2016 Introduction au calcul parallèle sous R
    * [Presentation](RParallel/RParallel.html)
    * [R Notebook - Code](RParallel/codeRParallel.Rmd)

